
var snow = [];

function createSnow(parentDiv, xStart, yStart, xEnd, yEnd, snows, speedMin, speedMax,  xSpeedMin, xSpeedMax) {
    snow.length = snows;
    for (var i = 0; i < snows; i++) {
        snow[i] = {
            div: document.createElement('div'),
            x: (xEnd-xStart)/snows*i,
            xSpeed: Math.floor(Math.random() * (xSpeedMax - xSpeedMin + 1)) + xSpeedMin,
            y: Math.floor(Math.random() * (yEnd - yStart + 1)) + yStart,
            ySpeed: Math.floor(Math.random() * (speedMax - speedMin + 1)) + speedMin
        };
        if(i%2==0)
          snow[i].xSpeed = -snow[i].xSpeed;    
        parentDiv.appendChild(snow[i].div);     
        snow[i].div.classList.add("snowFlake");     
        snow[i].div.style.width = Math.floor(Math.random() * (12 - 5 + 1)) + 5 + "px";
        snow[i].div.style.backgroundColor = "rgba(255, 255, 255, " + (Math.random() *  (0.9 - 0.2) + 0.2) + ")";
        snow[i].div.style.height = snow[i].div.style.width;
        snow[i].div.style.marginLeft = snow[i].x + "px";
        snow[i].div.style.marginTop = snow[i].y + "px";
       
    }
}


function update(i)
{
    snow[i].div.style.marginLeft = snow[i].x + "px";    
    snow[i].div.style.marginTop = snow[i].y + "px";
}

function moveSnow(snows, parentDiv, xStart, xEnd)
{
    parentDiv.style.height = parentDiv.parentNode.offsetHeight + "px";
 
    var yStart = parentDiv.offsetTop;
    var yEnd = parentDiv.offsetHeight;
    if(parentDiv.offsetWidth > xEnd) xEnd = parentDiv.offsetWidth; 

    for(var i=0; i<snows;i++)
    {
        if(snow[i].y>=yEnd-31)       
                snow[i].y = yStart;
    
        if(snow[i].x>xEnd)         
            snow[i].x = xStart;

        if(snow[i].x<xStart)        
            snow[i].x = xEnd;
        
        snow[i].y += (snow[i].ySpeed/10);
        snow[i].x += (snow[i].xSpeed/10);
      
               
            
            
            update(i);

    }
}
function go(parentDiv, snows, speedMin,  speedMax, xSpeedMin, xSpeedMax)
{   
    parentDiv.style.height = parentDiv.parentNode.offsetHeight + "px";
    var xStart = parentDiv.offsetLeft;
    var yStart = parentDiv.offsetTop;
    var xEnd = parentDiv.offsetWidth;
    var yEnd = parentDiv.offsetHeight;
    
    createSnow(parentDiv, xStart, yStart, xEnd, yEnd, snows, speedMin, speedMax,  xSpeedMin, xSpeedMax);
    
    setInterval(function() 
                {
                   
                    moveSnow(snows, parentDiv, xStart, xEnd);                    
                }, 10);    
}

